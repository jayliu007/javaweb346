package com.iweb.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author jay
 * @date 2023/3/26
 * @description 保护受限资源的自定义过滤器
 */
@WebFilter(filterName = "adminFilter", urlPatterns = "/admin/*")
public class AdminFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 判断当前会话中是否有登录凭证
        Object adminUser = session.getAttribute("adminUser");
        // 没有登录凭证
        if (adminUser == null) {
            // 要重新登录-重定向
            // 获取服务器项目的根目录
            String contextPath = request.getServletContext().getContextPath();
            response.sendRedirect(contextPath + "/day3/login.jsp");
            return;
        }
        // 有凭证，就继续传递请求
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
