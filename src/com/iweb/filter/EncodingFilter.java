package com.iweb.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author jay
 * @date 2023/3/26
 * @description 自定义编码过滤器
 */
@WebFilter(filterName = "encodingFilter",
        urlPatterns = {"*.do", "*.jsp"})
public class EncodingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("utf-8");
        // 请求继续向下一个过滤器或资源传递
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
