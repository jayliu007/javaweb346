package com.iweb.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author jay
 * @date 2023/3/24
 * @description 自定义servlet
 */
public class HelloServlet2 extends HttpServlet {
    public HelloServlet2() {
        System.out.println("HelloServlet被实例化了");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置响应编码
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 输出html元素与内容
        out.write("<html>\n");
        out.write("  <head><title>Servlet</title></head>\n");
        out.write("  <body>\n");
        out.write("你好，欢迎来到Servlet世界2");
        out.write("  </body>\n");
        out.write("</html>");
        out.close();
        System.out.println("调用了HelloServlet的service()方法");
        // 假设需要用户名和密码, 获取配置的初始化参数
        String username = this.getInitParameter("username");
        String password = this.getInitParameter("password");
        System.out.println(username + ":" + password);
        // 获取上下文参数
        String shareData = this.getServletContext().getInitParameter("shareData");
        System.out.println("HelloServlet获取到了系统上下文参数值：" + shareData);
    }
}
