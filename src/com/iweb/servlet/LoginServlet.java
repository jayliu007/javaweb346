package com.iweb.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author jay
 * @date 2023/3/24
 * @description
 */
public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 处理登录表单-- 转发模式
//        request.setCharacterEncoding("utf-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // todo: 使用jdbc验证
        // 假设合法的用户名密码 admin/123456
        if ("admin".equals(username)
                && "123456".equals(password)) {
            // 保存用户的登录信息到客户端本地cookie中
            Cookie cookie = new Cookie("validUser", username);
//    设置有效期60秒
            cookie.setMaxAge(3 * 60);
            response.addCookie(cookie);
            response.sendRedirect("day2_1/show.jsp");
        } else {
            // 若验证失败，1.设置失败信息
            request.setAttribute("errorMsg", "用户名或密码有误");
            // 2.要重新打开登录页面-转发
            request.getRequestDispatcher("day2_1/login.jsp").forward(request, response);
        }
    }
}
