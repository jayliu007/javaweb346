package com.iweb.servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

/**
 * @author jay
 * @date 2023/3/24
 * @description
 */
@WebServlet(name = "regServlet", urlPatterns = {"/regServlet.do"})
public class RegServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();

        boolean multipartContent = ServletFileUpload.isMultipartContent(req);
        if (!multipartContent) {
            req.setAttribute("fileErrorMsg", "表单格式有误");
            req.getRequestDispatcher("day2/reg.jsp").forward(req, resp);
            return;
        }
        // 创建FileItemFactory对象
        FileItemFactory factory = new DiskFileItemFactory();
        // ServletFileUpload对象，解析表单元素
        ServletFileUpload fileUpload = new ServletFileUpload(factory);
        // 解析表单元素
        try {
            List<FileItem> fileItems = fileUpload.parseRequest(req);
            // 处理每个表单元素
            for (FileItem fileItem : fileItems) {
                if (fileItem.isFormField()) {
                    // 普通元素
                    System.out.println(fileItem.getFieldName() + ":" + fileItem.getString());
                } else {
                    // 文件元素
//                    获取文件名
                    String fileName = fileItem.getName();
                    System.out.println("准备保存文件：" + fileName);
                    // 设置保存目录
                    String path = this.getServletContext().getRealPath("photo/");
                    File filePath = new File(path);
                    // 创建文件夹
                    if (!filePath.exists()) {
                        filePath.mkdirs();
                    }
                    // 创建保存文件的对象，文件保存前要改名,防止同名文件覆盖
                    fileName = UUID.randomUUID() + fileName;
                    File saveFile = new File(path, fileName);
                    fileItem.write(saveFile);
                    System.out.println("文件保存成功！");
                    writer.write("<p>文件" + fileName + "保存成功！</p>");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("errorMsg", e.getMessage());
            req.getRequestDispatcher("day2/reg.jsp").forward(req, resp);
        }
    }
}
