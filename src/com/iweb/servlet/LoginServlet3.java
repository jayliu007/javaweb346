package com.iweb.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author jay
 * @date 2023/3/24
 * @description
 */
@WebServlet(name = "loginServlet3", urlPatterns = {"/loginServlet3.do"})
public class LoginServlet3 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // 假设合法的用户名密码 admin/123456
        if ("admin".equals(username)
                && "123456".equals(password)) {
            // 写入授权凭证
            request.getSession().setAttribute("adminUser", username);
            response.sendRedirect("admin/index.jsp");
        } else {
            // 若验证失败，1.设置失败信息
            request.setAttribute("errorMsg", "用户名或密码有误");
            // 2.要重新打开登录页面-转发
            request.getRequestDispatcher("day3/login.jsp").forward(request, response);
        }
    }
}
