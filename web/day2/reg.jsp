<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>用户注册</title>
</head>
<body>
<h2>用户注册</h2>
<form action="../regServlet.do" method="post" enctype="multipart/form-data">
  <p>用户名：<input type="text" name="username"></p>
  <p>密码：<input type="password" name="password"></p>
  <p>性别：
    <input id="male" type="radio" name="sex" value="1" checked>
    <label for="male">男</label>
    <input id="female" type="radio" name="sex" value="0">
    <label for="female">女</label>
  </p>
  <p>兴趣爱好：
    <input type="checkbox" name="hobby" value="basketball">蓝球
    <input type="checkbox" name="hobby" value="sing">唱歌
    <input type="checkbox" name="hobby" value="dance">跳舞
  </p>
  <p>身份照片：
    <input type="file" name="photo">
  </p>
  <p>
    <button>注册</button>
    <input type="reset" value="重填">
  </p>
</form>
</body>
</html>
