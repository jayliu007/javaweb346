<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
</head>
<body>
<%
  String username = "";
  // 获取浏览器发送过来的cookie对象
  Cookie[] cookies = request.getCookies();
  if (cookies != null) {
    for (Cookie cookie : cookies) {
      if (cookie.getName().equals("username")) {
        username = cookie.getValue();
      }
    }
  }
%>
<p>获取到的cookie数据：<%=username%>
</p>
</body>
</html>
