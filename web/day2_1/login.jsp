<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  // 利用cookie实现已登录的用户的免登录案例
  Cookie[] cookies = request.getCookies();
  if (cookies != null) {
    for (Cookie cookie : cookies) {
      if (cookie.getName().equals("validUser")) {
        // 找到已登录的标识符后，免登录，直接跳转到首页
        response.sendRedirect("show.jsp");
        return;
      }
    }
  }

  // 获取转发的失败信息
  String errorMsg = request.getAttribute("errorMsg") == null
          ? "" : request.getAttribute("errorMsg").toString();
%>
<html>
<head>
  <title>用户登录</title>
</head>
<body>
<%--<form method="post" action="doLogin.jsp">--%>
<form method="post" action="../loginServlet.do">
  <p>用户名：<input type="text" name="username"></p>
  <p>密码：<input type="password" name="password"></p>
  <p><%=errorMsg%>
  </p>
  <p>
    <button>登录</button>
  </p>
</form>
</body>
</html>
