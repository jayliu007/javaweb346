<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  Integer counter = (Integer) application.getAttribute("counter");
  // 第一次打开页面
  if (counter == null) {
    counter = 1;
  } else {
    counter++;
  }
  // 保存counter
  application.setAttribute("counter", counter);
%>

<html>
<head>
  <title>Title</title>
</head>
<body>
<p>当前页面访问次数：<%=counter%>
</p>
</body>
</html>
