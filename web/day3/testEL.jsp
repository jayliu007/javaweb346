<%@ page import="com.iweb.entity.User" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  request.setAttribute("user1", "tom1");
  session.setAttribute("user2", "tom2");
  User user3 = new User(3, "tom3", 1);
  request.setAttribute("user3", user3);
  // 保存集合
  List<User> userList = new ArrayList<>();
  userList.add(new User(1, "aa", 1));
  userList.add(new User(2, "bb", 1));
  userList.add(new User(3, "cc", 1));
  request.setAttribute("userList", userList);

  Map<String, User> userMap = new HashMap<>();
  userMap.put("one", new User(1, "aa", 1));
  userMap.put("two", new User(2, "bb", 1));
  userMap.put("three", new User(3, "cc", 1));
  request.setAttribute("userMap", userMap);
%>
<html>
<head>
  <title>Title</title>
</head>
<body>
<p>user1：${requestScope.user1}</p>
<p>user2：${sessionScope.user2}</p>
<p>user3：${requestScope.user3.name}</p>
<p>user3：${requestScope.user3["id"]}</p>
<p>user4: ${requestScope.userList[1].name}</p>
<p>user5: ${requestScope.userMap["three"].name}</p>
<%--使用jstl设置变量值--%>
<c:set var="user6" value="tom6" scope="request"/>
<p>user6：${requestScope.user6}</p>
<p>user7: <c:out value="${requestScope.user7}" default="游客"/></p>
<%--删除user1--%>
<c:remove var="user1" scope="request"/>
<p>user1：${requestScope.user1}</p>
<c:if test="${ empty requestScope.user1}">
  <p>user1被删除了</p>
</c:if>
<%--实现输出成绩的A-E等级--%>
<c:set var="score" value="55" scope="request"/>
<c:choose>
  <c:when test="${requestScope.score>=90}">
    <p>A</p>
  </c:when>
  <c:when test="${requestScope.score>=80}">
    <p>B</p>
  </c:when>
  <c:when test="${requestScope.score>=70}">
    <p>C</p>
  </c:when>
  <c:when test="${requestScope.score>=60}">
    <p>D</p>
  </c:when>
  <c:otherwise>
    <p>E</p>
  </c:otherwise>
</c:choose>
<p>用户列表</p>
<ul>
  <c:forEach items="${requestScope.userList}" var="user">
    <li>${user.id} - ${user.name}</li>
  </c:forEach>
</ul>
</body>
</html>
