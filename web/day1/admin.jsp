<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  // 判断当前会话中是否有登录凭证
  Object loginUser = session.getAttribute("loginUser");
  // 没有登录凭证
  if (loginUser == null) {
    // 要重新登录-重定向
    response.sendRedirect("login.jsp");
    return;
  }
%>

<html>
<head>
  <title>Title</title>
</head>
<body>
<h1>xx后台管理系统</h1>
<p>管理员<%=loginUser%>,欢迎你！</p>
</body>
</html>
