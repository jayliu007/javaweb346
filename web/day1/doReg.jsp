<%@ page import="java.nio.charset.Charset" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  // 设置编码，以解决乱码问题
  request.setCharacterEncoding("utf-8");
  //  处理表单请求
  String username = request.getParameter("username");
  String password = request.getParameter("password");
  String sex = request.getParameter("sex");
  String[] hobbies = request.getParameterValues("hobby");
%>

<html>
<head>
  <title>Title</title>
</head>
<body>
<%--  输出注册信息--%>
<p>你的注册信息如下：</p>
<ul>
  <li>用户名：<%=username%>
  </li>
  <li>密码：<%=password%>
  </li>
  <li>性别：<%=sex%>
  </li>
  <li>兴趣爱好：
    <%
      if (hobbies != null) {
        for (int i = 0; i < hobbies.length; i++) {
    %>
    <%=hobbies[i]%>
    <%
        }
      }
    %>
  </li>
</ul>
</body>
</html>
