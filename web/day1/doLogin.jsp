<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  // 处理登录表单
  request.setCharacterEncoding("utf-8");
  String username = request.getParameter("username");
  String password = request.getParameter("password");

  // todo: 使用jdbc验证
  // 假设合法的用户名密码 admin/123456
  if ("admin".equals(username)
          && "123456".equals(password)) {
    // 跳转到管理员页面
    response.sendRedirect("admin.jsp");
  } else {
    // 若验证失败，要重新打开登录页面
    response.sendRedirect("login.jsp");
  }
%>