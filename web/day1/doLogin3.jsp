<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  // 处理登录表单-- 转发模式
  request.setCharacterEncoding("utf-8");
  String username = request.getParameter("username");
  String password = request.getParameter("password");

  // todo: 使用jdbc验证
  // 假设合法的用户名密码 admin/123456
  if ("admin".equals(username)
          && "123456".equals(password)) {
    // 设置登录验证通过后的凭证
    session.setAttribute("loginUser", username);
    // 可以使用重定向到后台首页了
    response.sendRedirect("admin.jsp");
  } else {
    // 若验证失败，1.设置失败信息
    request.setAttribute("errorMsg", "用户名或密码有误");
    // 2.要重新打开登录页面-转发
    request.getRequestDispatcher("login.jsp").forward(request, response);
  }
%>